
// SimpleScannerDlg.h : header file
//

#pragma once
#include <string>
#include <vector>

//self defined message
#define WM_COMPLETE (WM_USER + 1)


// CSimpleScannerDlg dialog
class CSimpleScannerDlg : public CDialogEx
{
// Construction
public:
	CSimpleScannerDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SIMPLESCANNER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	void LockControls();
	void UnlockControls();
public:
	CString m_strPathName;
	CString m_strSignature;
	CEdit m_ctrlStatus;
	CString m_strStatus;

	int nb_max_solve_thread;
	int cur_running_solve_thread;
	bool isGetTaskDone;

	std::vector<CString> task{};
	std::vector<CString> result{};

	afx_msg void OnBnClickedButtonBrowse();
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnBnClickedButtonStart();
	BOOL isValidSignature();

	void refresh();
	void appendStatus(CString text);
	void getTask(CString path);

	static UINT mainWorkerThreadProc(LPVOID Param);
	static UINT getTaskThreadProc(LPVOID Param);
	static UINT solveTaskThreadProc(LPVOID Param);
	static INT scanFile(CString path, CString signature);
	static UINT handleScanFileReturn(LPVOID Param, CString task);

	afx_msg LRESULT CSimpleScannerDlg::OnCompleteGetTask(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT CSimpleScannerDlg::OnComplete(WPARAM wParam, LPARAM lParam);
};

std::wstring string_to_hex(const std::wstring& input);
int hex_value(unsigned char hex_digit);
std::wstring hex_to_string(const std::wstring& input);

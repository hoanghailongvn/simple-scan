﻿
// SimpleScannerDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "SimpleScanner.h"
#include "SimpleScannerDlg.h"
#include "afxdialogex.h"

#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <mutex>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

std::mutex mtx_task;
std::mutex mtx_result;
std::mutex mtx_isGetTaskDone;
std::mutex mtx_cur_running_solve_thread;
std::mutex mtx_appendStatus;

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CSimpleScannerDlg dialog



CSimpleScannerDlg::CSimpleScannerDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SIMPLESCANNER_DIALOG, pParent)
	, m_strPathName(_T("C:\\"))
	, m_strSignature(_T("48656c6c6f"))
	, m_strStatus(_T(""))
	, isGetTaskDone(false)
	, nb_max_solve_thread(3)
	, cur_running_solve_thread(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSimpleScannerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PATH, m_strPathName);
	DDX_Text(pDX, IDC_EDIT_SIGNATURE, m_strSignature);
	DDX_Control(pDX, IDC_EDIT_STATUS, m_ctrlStatus);
	DDX_Text(pDX, IDC_EDIT_STATUS, m_strStatus);
}

BEGIN_MESSAGE_MAP(CSimpleScannerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_BROWSE, &CSimpleScannerDlg::OnBnClickedButtonBrowse)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CSimpleScannerDlg::OnBnClickedButtonClose)
	ON_BN_CLICKED(IDC_BUTTON_START, &CSimpleScannerDlg::OnBnClickedButtonStart)
	ON_MESSAGE(WM_COMPLETE, &CSimpleScannerDlg::OnComplete)
END_MESSAGE_MAP()


// CSimpleScannerDlg message handlers

BOOL CSimpleScannerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSimpleScannerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSimpleScannerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSimpleScannerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSimpleScannerDlg::appendStatus(CString text) {
	mtx_appendStatus.lock();
	
	// 1
	//int length = m_ctrlStatus.GetWindowTextLengthW();
	//m_ctrlStatus.SetSel(length, length);
	//m_ctrlStatus.ReplaceSel(text);

	// 2
	this->m_strStatus += text;
	this->m_ctrlStatus.SetWindowText(this->m_strStatus);
	this->m_ctrlStatus.LineScroll(m_ctrlStatus.GetLineCount());

	mtx_appendStatus.unlock();
}


void CSimpleScannerDlg::OnBnClickedButtonBrowse()
{
	// TODO: Add your control notification handler code here
	CFolderPickerDialog dlg;
	dlg.m_ofn.lpstrTitle = _T("Put your title here");
	dlg.m_ofn.lpstrInitialDir = _T("C:\\");
	if (IDOK == dlg.DoModal())
	{
		this->m_strPathName = dlg.GetPathName();
		UpdateData(FALSE);
	}
}

void CSimpleScannerDlg::OnBnClickedButtonClose()
{
	// TODO: Add your control notification handler code here
	CDialog::OnOK();
}

void CSimpleScannerDlg::LockControls() {
	this->GetDlgItem(IDC_EDIT_PATH)->EnableWindow(FALSE);
	this->GetDlgItem(IDC_EDIT_SIGNATURE)->EnableWindow(FALSE);
	this->GetDlgItem(IDC_BUTTON_BROWSE)->EnableWindow(FALSE);
	this->GetDlgItem(IDC_BUTTON_START)->EnableWindow(FALSE);
}

void CSimpleScannerDlg::UnlockControls() {
	this->GetDlgItem(IDC_EDIT_PATH)->EnableWindow();
	this->GetDlgItem(IDC_EDIT_SIGNATURE)->EnableWindow();
	this->GetDlgItem(IDC_BUTTON_BROWSE)->EnableWindow();
	this->GetDlgItem(IDC_BUTTON_START)->EnableWindow();
}

void CSimpleScannerDlg::OnBnClickedButtonStart()
{
	// TODO: Add your control notification handler code here
	this->LockControls();

	UpdateData();

	this->refresh();
	
	if (!PathIsDirectory(this->m_strPathName)) {
		AfxMessageBox(_T("The system cannot find the file specified."));
		this->UnlockControls();
	}
	else if (!this->isValidSignature()) {
		AfxMessageBox(_T("Invalid signature!"));
		this->UnlockControls();
	}
	else {
		CString begin{};
		begin += _T("Chương trình bắt đầu quét từ ") + CTime::GetCurrentTime().Format("%H:%M:%S %m/%d/%Y");
		begin += _T(" tại thư mục ") + this->m_strPathName + _T("\r\n");

		this->appendStatus(begin);

		AfxBeginThread(CSimpleScannerDlg::mainWorkerThreadProc, this);	
	}
}

UINT CSimpleScannerDlg::mainWorkerThreadProc(LPVOID Param) {
	CSimpleScannerDlg* ourWnd = reinterpret_cast<CSimpleScannerDlg*>(Param);

	ourWnd->isGetTaskDone = false;
	AfxBeginThread(CSimpleScannerDlg::getTaskThreadProc, ourWnd);

	ourWnd->cur_running_solve_thread = 0;
	for (int i = 0; i < ourWnd->nb_max_solve_thread; i++) {
		mtx_cur_running_solve_thread.lock();
		ourWnd->cur_running_solve_thread++;
		mtx_cur_running_solve_thread.unlock();

		AfxBeginThread(CSimpleScannerDlg::solveTaskThreadProc, ourWnd);
	}

	return 0;
}

UINT CSimpleScannerDlg::getTaskThreadProc(LPVOID Param) {
	CSimpleScannerDlg* ourWnd = reinterpret_cast<CSimpleScannerDlg*>(Param);

	ourWnd->getTask(ourWnd->m_strPathName);

	mtx_isGetTaskDone.lock();
	ourWnd->isGetTaskDone = true;
	mtx_isGetTaskDone.unlock();
	return 0;
}

void CSimpleScannerDlg::getTask(CString path) {
	CFileFind finder;
	BOOL bWorking = finder.FindFile(path + _T("\\*"));

	while (bWorking) {
		bWorking = finder.FindNextFile();

		//skip .. and .
		if (!finder.IsDots()) {
			if (PathIsDirectory(finder.GetFilePath())) {
				this->getTask(finder.GetFilePath());
			}
			else {
				mtx_task.lock();
				this->task.push_back(finder.GetFilePath());
				mtx_task.unlock();
			}
		}
	}
}

UINT CSimpleScannerDlg::solveTaskThreadProc(LPVOID Param) {
	CSimpleScannerDlg* ourWnd = reinterpret_cast<CSimpleScannerDlg*>(Param);
	CString cur_task;
	bool have_task;

	while (1) {
		have_task = false;

		//get task from queue
		mtx_task.lock();
		if (ourWnd->task.size() > 0) {
			have_task = true;
			cur_task = ourWnd->task.back();
			ourWnd->task.pop_back();
		}
		mtx_task.unlock();

		//solve task if have
		if (have_task) {
			CSimpleScannerDlg::handleScanFileReturn(ourWnd, cur_task);
			continue;
		}

		//if queue empty and no more task, end thread
		mtx_isGetTaskDone.lock();
		if (ourWnd->isGetTaskDone) {
			mtx_isGetTaskDone.unlock();
			break;
		}
		else {
			mtx_isGetTaskDone.unlock();
		}
	}

	mtx_cur_running_solve_thread.lock();
	ourWnd->cur_running_solve_thread--;
	if (ourWnd->cur_running_solve_thread == 0) {
		mtx_cur_running_solve_thread.unlock();
		ourWnd->PostMessageW(WM_COMPLETE);
	}
	else {
		mtx_cur_running_solve_thread.unlock();
	}

	return 0;
}

// input: path to file, signature
// return:
// -1: file size > 50mb
// -0: file do not contains signature
// 1: file contains signature
INT CSimpleScannerDlg::scanFile(CString path, CString CStr_signature) {
	std::ifstream file;
	file.open(path, std::fstream::in);

	if (file.is_open()) {
		// check size
		file.seekg(0, std::ios::end);
		UINT64 file_size = file.tellg();
		if (file_size > 50'000'000) {
			return -1;
		}

		// check signature
		file.seekg(0, std::ios::beg);
		std::istream_iterator<char> begin(file), end;

		// convert signature hex string to hex
		std::wstring wstr_signature(CStr_signature);
		std::wstring signature = hex_to_string(wstr_signature);
		if (std::search(begin, end, signature.begin(), signature.end()) != end) {
			return 1;
		}
		else {
			return 0;
		}
	}
	return false;
}

UINT CSimpleScannerDlg::handleScanFileReturn(LPVOID Param, CString task) {
	CSimpleScannerDlg* ourWnd = reinterpret_cast<CSimpleScannerDlg*>(Param);

	INT res = CSimpleScannerDlg::scanFile(task, ourWnd->m_strSignature);
	switch (res)
	{
	case -1:
		ourWnd->appendStatus(_T("[!] File ") + task + _T(" có kích thước quá lớn; bỏ qua\r\n"));
		break;
	case 0:
		ourWnd->appendStatus(_T("[-] Đã quét xong file ") + task + _T("\r\n"));
		break;
	case 1:
		ourWnd->appendStatus(_T("[+] Đã quét xong file ") + task + _T(", tìm thấy signature trong file!\r\n"));
		mtx_result.lock();
		ourWnd->result.push_back(task);
		mtx_result.unlock();
		break;
	default:
		break;
	}

	return 0;

}

BOOL CSimpleScannerDlg::isValidSignature() {
	CString signature = this->m_strSignature;

	if (std::regex_match(signature.GetString()
		, signature.GetString() + signature.GetLength()
		, std::regex("^([0-9A-Fa-f]{2})+$"))) {
		
		return true;
	}
	else {
		return false;
	}
}

afx_msg	LRESULT CSimpleScannerDlg::OnComplete(WPARAM wParam, LPARAM lParam) {
	CString f_result{};
	f_result += _T("Chương trình đã quét xong vào ") + CTime::GetCurrentTime().Format("%H:%M:%S %m/%d/%Y");
	f_result += _T(", tìm thấy ") + (CString)(std::to_wstring(this->result.size()).c_str()) + _T(" file chứa signature.\r\n");
	
	if (!this->result.empty()) {
		f_result += _T("Danh sách các file tìm được:\r\n");
		for (int i = 0; i < this->result.size(); i++) {
			f_result += this->result.at(i) + _T("\r\n");
		}
	}

	this->appendStatus(f_result);

	CString done_msg = _T("Chương trình đã quét xong, tìm thấy ");
	done_msg += (CString)(std::to_wstring(this->result.size()).c_str());
	done_msg += _T(" file chứa signature!");

	AfxMessageBox(done_msg, MB_OK);
	this->UnlockControls();

	return 0;
}

void CSimpleScannerDlg::refresh() {
	// refresh variable
	this->cur_running_solve_thread = 0;
	this->isGetTaskDone = false;
	this->task.clear();
	this->result.clear();
	
	// refresh log
	this->m_strStatus = _T("");

}

std::wstring string_to_hex(const std::wstring& input)
{
	static const char hex_digits[] = "0123456789ABCDEF";

	std::wstring output;
	output.reserve(input.length() * 2);
	for (unsigned char c : input)
	{
		output.push_back(hex_digits[c >> 4]);
		output.push_back(hex_digits[c & 15]);
	}
	return output;
}

int hex_value(unsigned char hex_digit)
{
	static const signed char hex_values[256] = {
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, -1, -1, -1, -1, -1, -1,
		-1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, 10, 11, 12, 13, 14, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	};
	int value = hex_values[hex_digit];

	//checked in isValidSignature
	//if (value == -1) throw std::invalid_argument("invalid hex digit");
	return value;
}

std::wstring hex_to_string(const std::wstring& input)
{
	const auto len = input.length();

	//checked in isValidSignature
	//if (len & 1) throw std::invalid_argument("odd length");

	std::wstring output;
	output.reserve(len / 2);
	for (auto it = input.begin(); it != input.end(); )
	{
		int hi = hex_value(*it++);
		int lo = hex_value(*it++);
		output.push_back(hi << 4 | lo);
	}
	return output;
}